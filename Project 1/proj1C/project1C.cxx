#include <iostream>
#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
using std::cerr;
using std::endl;

double ceil_441(double f)
{
    return ceil(f-0.00001);
}

double floor_441(double f)
{
    return floor(f+0.00001);
}


vtkImageData *
NewImage(int width, int height)
{
    vtkImageData *img = vtkImageData::New();
    img->SetDimensions(width, height, 1);
    img->AllocateScalars(VTK_UNSIGNED_CHAR, 3);

    return img;
}

void
WriteImage(vtkImageData *img, const char *filename)
{
   std::string full_filename = filename;
   full_filename += ".png";
   vtkPNGWriter *writer = vtkPNGWriter::New();
   writer->SetInputData(img);
   writer->SetFileName(full_filename.c_str());
   writer->Write();
   writer->Delete();
}

class Triangle
{
  public:
      double         X[3];
      double         Y[3];
      unsigned char color[3];

};

class Screen
{
  public:
      unsigned char   *buffer;
      int width, height;

};

std::vector<Triangle>
GetTriangles(void)
{
    vtkPolyDataReader *rdr = vtkPolyDataReader::New();
    rdr->SetFileName("proj1c_geometry.vtk");
    cerr << "Reading" << endl;
    rdr->Update();
    if (rdr->GetOutput()->GetNumberOfCells() == 0)
    {
        cerr << "Unable to open file!!" << endl;
        exit(EXIT_FAILURE);
    }
    vtkPolyData *pd = rdr->GetOutput();
    int numTris = pd->GetNumberOfCells();
    vtkPoints *pts = pd->GetPoints();
    vtkCellArray *cells = pd->GetPolys();
    vtkFloatArray *colors = (vtkFloatArray *) pd->GetPointData()->GetArray("color_nodal");
    float *color_ptr = colors->GetPointer(0);
    std::vector<Triangle> tris(numTris);
    vtkIdType npts;
    vtkIdType *ptIds;
    int idx;
    for (idx = 0, cells->InitTraversal() ; cells->GetNextCell(npts, ptIds) ; idx++)
    {
        if (npts != 3)
        {
            cerr << "Non-triangles!! ???" << endl;
            exit(EXIT_FAILURE);
        }
        tris[idx].X[0] = pts->GetPoint(ptIds[0])[0];
        tris[idx].X[1] = pts->GetPoint(ptIds[1])[0];
        tris[idx].X[2] = pts->GetPoint(ptIds[2])[0];
        tris[idx].Y[0] = pts->GetPoint(ptIds[0])[1];
        tris[idx].Y[1] = pts->GetPoint(ptIds[1])[1];
        tris[idx].Y[2] = pts->GetPoint(ptIds[2])[1];
        tris[idx].color[0] = (unsigned char) color_ptr[4*ptIds[0]+0];
        tris[idx].color[1] = (unsigned char) color_ptr[4*ptIds[0]+1];
        tris[idx].color[2] = (unsigned char) color_ptr[4*ptIds[0]+2];
    }
    cerr << "Done reading" << endl;

    return tris;
}
void RasterizeGoingUpTriangle(Triangle triangle, unsigned char *buffer){
    double leftX;
    double leftY;
    double rightX;
    double rightY;
    double topX;
    double topY;

    if(triangle.Y[0] == triangle.Y[1]){
        rightY = triangle.Y[0];
        leftY  = triangle.Y[0];
        if(triangle.X[0] < triangle.X[1]){
            leftX  = triangle.X[0];
            rightX = triangle.X[1];
        }
        else{
            leftX  = triangle.X[1];
            rightX = triangle.X[0];
        }
        topX = triangle.X[2];
        topY = triangle.Y[2];
    }
    else if(triangle.Y[0] == triangle.Y[2]){
        rightY = triangle.Y[0];
        leftY  = triangle.Y[0];
        if(triangle.X[0] < triangle.X[2]){
            leftX  = triangle.X[0];
            rightX = triangle.X[2];
        }
        else{
            leftX  = triangle.X[2];
            rightX = triangle.X[0];
        }
        topX = triangle.X[1];
        topY = triangle.Y[1];
    }
    else{
        rightY = triangle.Y[1];
        leftY  = triangle.Y[1];
        if(triangle.X[1] < triangle.X[2]){
            leftX  = triangle.X[1];
            rightX = triangle.X[2];
        }
        else{
          leftX  = triangle.X[2];
          rightX = triangle.X[1];
        }
        topX = triangle.X[0];
        topY = triangle.Y[0];
    }

    int ymin = ((leftY < 0)    ?   0  : ceil_441(leftY));
    int ymax = ((topY >= 1344) ? 1343 : floor_441(topY));

    double slope_l = ((topX == leftX)  ?  0.0 : (topY - leftY) / (topX - leftX));
    double slope_r = ((topX == rightX) ?  0.0 : (rightY - topY) / (rightX - topX));


    for(int i = ymin; i <= ymax; i++){
        int r_bound;
        int l_bound;
        int cur_leftX;
        int cur_rightX;

        cur_leftX = ((slope_l != 0.0) ? ceil_441(leftX + (((double) i) - leftY)/slope_l) : ceil_441(leftX));
        l_bound = ((cur_leftX < 0) ? 0 : cur_leftX);
        cur_rightX = ((slope_r != 0.0) ? rightX + ((((double) i) - leftY)/slope_r) : rightX);
        r_bound = ((cur_rightX >= 1786) ? 1785 : cur_rightX);

        for(int j = l_bound; j <= r_bound; j++){

            int index = 3*(1786*i+j);
            buffer[index] = triangle.color[0];
            buffer[index + 1] = triangle.color[1];
           buffer[index + 2] = triangle.color[2];
         }
    }

}


void RasterizeGoingDownTriangle(Triangle triangle, unsigned char *buffer)
{
   double leftX;
   double leftY;
   double rightX;
   double rightY;
   double topX;
   double topY;// "top" (really the bottom) of the triangle (the bottom point)

   if(triangle.Y[0] == triangle.Y[1])
   {
      rightY = triangle.Y[0];
      leftY  = triangle.Y[0];

      if(triangle.X[0] < triangle.X[1])
      {
         leftX  = triangle.X[0];
         rightX = triangle.X[1];
      }
      else{
         leftX  = triangle.X[1];
         rightX = triangle.X[0];
      }
       topX = triangle.X[2];
       topY = triangle.Y[2];
    }
    else if(triangle.Y[0] == triangle.Y[2])
    {
       rightY = triangle.Y[0];
       leftY  = triangle.Y[0];

       if(triangle.X[0] < triangle.X[2]){
          leftX  = triangle.X[0];
          rightX = triangle.X[2];
       }
       else{
          leftX  = triangle.X[2];
          rightX = triangle.X[0];
       }
       topX = triangle.X[1];
       topY = triangle.Y[1];
    }
    else{
       rightY = triangle.Y[1];
       leftY  = triangle.Y[1];

       if(triangle.X[1] < triangle.X[2])
       {
          leftX  = triangle.X[1];
          rightX = triangle.X[2];
       }
       else{
          leftX  = triangle.X[2];
          rightX = triangle.X[1];
       }
       topX = triangle.X[0];
       topY = triangle.Y[0];
    }

    int ymin, ymax;
    //if topY > 0: set ymin equal to 0 else: set ymin equal to ceiling(topY)
    ymin = ((topY<0) ? 0 : ceil_441(topY));

    //if leftY > 999: set ymax = 999 else: set ymax to floor(leftY)
    ymax = ((leftY >= 1344) ? 1343 : floor_441(leftY));


    double slope_l;
    double slope_r;

    // if topX == leftX: set left slope = 0.0. Else: set left slope equal to function
    slope_l = (( topX == leftX) ? 0.0 : ((topY - leftY)/(topX - leftX)));

    // if topX is equal to rightX: set right slope to 0.0. Else: set it equal to function.
    slope_r = (( topX == rightX) ? 0.0 : ((rightY - topY)/(rightX - topX)));


    for(int i = ymin; i <= ymax; i++){
       int r_bound;
       int l_bound;
       int cur_leftX;
       int cur_rightX;
       double tmp1;
       double tmp2;

       if(slope_l != 0.0){
          tmp1 = (((double) i) - topY)/(slope_l);
          cur_leftX = ceil_441(topX + tmp1);
       }
       else{
          cur_leftX = ceil_441(leftX);
       }

       //if the currently leftX point is negative, set to 0. otherwise set to current leftX point
       l_bound = ((cur_leftX < 0) ? 0 : cur_leftX);

       if(slope_r != 0.0){
          tmp2 = (((double) i) - topY)/(slope_r);
          cur_rightX = floor_441(topX + tmp2);
       }
       else{
          cur_rightX = floor_441(rightX);
       }

       r_bound = ((cur_rightX >= 1786) ? 1785 : cur_rightX);

       for(int j = l_bound; j <= r_bound; j++){
          int index = 3*(1786*i + j); //special formula ;)
          //int index = 3*pix;    //dont forget the 3 colors!
          buffer[index] = triangle.color[0];    //r
          buffer[index + 1] = triangle.color[1];//g
          buffer[index + 2] = triangle.color[2];//b
        }
    }
}
int main()
{
   cerr << "In Main..." << endl;
   vtkImageData *image = NewImage(1786, 1344);
   unsigned char *buffer =
     (unsigned char *) image->GetScalarPointer(0,0,0);
   int npixels = 1786*1344;
   for (int i = 0 ; i < npixels*3 ; i++)
       buffer[i] = 0;

   std::vector<Triangle> triangles = GetTriangles();

   Screen screen;
   screen.buffer = buffer;
   screen.width = 1786;
   screen.height = 1344;

   double minY, midY, maxY, maxX, minX, midX;

   int size = triangles.size();
   for(int j = 0; j<size; j++){

    Triangle triangle = triangles[j];

    if(triangle.Y[0] == triangle.Y[1]){
      if(triangle.Y[2] > triangle.Y[0]){
        RasterizeGoingUpTriangle(triangle, buffer);
      }
      else{
        RasterizeGoingDownTriangle(triangle, buffer);
      }
    }
    else if(triangle.Y[0] == triangle.Y[2]){
      if(triangle.Y[1] > triangle.Y[0]){
         RasterizeGoingUpTriangle(triangle, buffer);
      }
      else{
        RasterizeGoingDownTriangle(triangle, buffer);
      }
    }
    else if(triangle.Y[1] == triangle.Y[2]){
      if(triangle.Y[0] > triangle.Y[1]){
        RasterizeGoingUpTriangle(triangle, buffer);
      }
      else{
        RasterizeGoingDownTriangle(triangle, buffer);
      }
    }
    else{
      minY = triangle.Y[0];
      minX = triangle.X[0];
      maxY = triangle.Y[0];
      maxX = triangle.X[0];
      midY = triangle.Y[0];
      midX = triangle.X[0];
      for(int i = 1; i<3; i++){
        if(triangle.Y[i] > maxY){
          maxY = triangle.Y[i];
          maxX = triangle.X[i];
        }
        if(triangle.Y[i] < minY){
          minY = triangle.Y[i];
          minX = triangle.X[i];
        }
      }

      for(int i = 0; i<3; i++){
        if((triangle.Y[i] < maxY) && (triangle.Y[i] > minY)){
          midY = triangle.Y[i];
          midX = triangle.X[i];
        }
      }

      double slope = (maxY-minY)/(maxX-minX);
      double intercept = slope*(-1*maxX) + maxY;
      double newX = (midY - intercept)/slope;

      std::vector<Triangle> rv(2);

      Triangle triangle1 = rv[0];
      Triangle triangle2 = rv[1];

      triangle1.Y[0] = maxY;
      triangle1.X[0] = maxX;
      triangle1.Y[1] = midY;
      triangle1.X[1] = midX;
      triangle1.Y[2] = midY;
      triangle1.X[2] = newX;
      triangle1.color[0] = triangle.color[0];
      triangle1.color[1] = triangle.color[1];
      triangle1.color[2] = triangle.color[2];

      triangle2.Y[0] = minY;
      triangle2.X[0] = minX;
      triangle2.Y[1] = midY;
      triangle2.X[1] = midX;
      triangle2.Y[2] = midY;
      triangle2.X[2] = newX;
      triangle2.color[0] = triangle.color[0];
      triangle2.color[1] = triangle.color[1];
      triangle2.color[2] = triangle.color[2];

      RasterizeGoingUpTriangle(triangle1, buffer);

      RasterizeGoingDownTriangle(triangle2, buffer);
    }

  }

   cerr << "writing image" << endl;
   WriteImage(image, "proj1C");
   cerr << "Finished." << endl;
}
