### Project 1C - Rasterize Screen-Space Triangles

Implementation of scanline algorithm to fill screen space with rasterized up and down triangles from a given file

Result should look like:

![result](https://bitbucket.org/joegroe/441-introduction-to-computer-graphics/raw/cb8ae4f5bf5aaaadbfaeb998e90ebee5e88ad89b/Project%201/proj1C/proj1C.png)

##Usage
```bash
cmake .
make
open proj1C.png
```
