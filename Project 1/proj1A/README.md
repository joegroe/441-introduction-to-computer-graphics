### Project 1A - Getting Started

Assignment was to get used to using cmake and VTK

Program runs and displays a color grid using RGB combinations

###Usage

```bash
cmake .
make
./project1A
```

Result should output:

![output](https://bitbucket.org/joegroe/441-introduction-to-computer-graphics/raw/ac280a780e42acadba63ed2e986fdb33b388ce58/Project%201/proj1A/allColors.png)
