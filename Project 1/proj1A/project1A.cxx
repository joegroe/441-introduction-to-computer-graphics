#include <iostream>
#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>

using std::cerr;
using std::endl;

vtkImageData *
NewImage(int width, int height)
{
    vtkImageData *img = vtkImageData::New();
    img->SetDimensions(width, height, 1);
    img->AllocateScalars(VTK_UNSIGNED_CHAR, 3);

    return img;
}

void
WriteImage(vtkImageData *img, const char *filename)
{
   std::string full_filename = filename;
   full_filename += ".png";
   vtkPNGWriter *writer = vtkPNGWriter::New();
   writer->SetInputData(img);
   writer->SetFileName(full_filename.c_str());
   writer->Write();
   writer->Delete();
}


int main()
{
   std::cerr << "In main!" << endl;
   //                            (width, height)
   vtkImageData *image = NewImage(1024 ,   1350);
   unsigned char *buffer = 
     (unsigned char *) image->GetScalarPointer(0,0,0);

   int i;
   int j;
   for(i = 0; i < 27; i++)
   {
      //equations for r,b,g pixel
      int red_l   = (i / 9);
      int green_l = (i / 3) % 3;
      int blue_l  = (i % 3);
      
      //variables to store color value
      int red;
      int green;
      int blue;
      

      //blue pixel
      if ( blue_l == 1)
      {
         blue = 128;
      } 
      else if ( blue_l == 2)
      {
         blue = 255;
      }
      else
      {
         blue = 0;
      }

      //green pixel
      if ( green_l == 1)
      {
         green = 128;
      } 
      else if ( green_l == 2)
      {
         green = 255;
      }
      else
      {
         green = 0;
      }

      //red pixel
      if ( red_l == 1)
      {
         red = 128;
      } 
      else if ( red_l == 2)
      {
         red = 255;
      }
      else
      {
         red = 0;
      }
      
      int index = 3*(50*1024); 

      for (j = 0; j < index; j+=3)
      {
         //print statement was to test logic above 
         //printf("red: %d, blue: %d, green: %d\n",red, blue, green);

         buffer[i * index + j]     = red;
         buffer[i * index + j + 1] = green;
         buffer[i * index + j + 2] = blue;
      }
   }
   WriteImage(image, "proj1A");
   // :)
}
