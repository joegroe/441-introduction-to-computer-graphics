#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <iostream>
#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkPNGWriter.h>

using std::cerr;
using std::endl;


double ceil_441(double f)
{
    return ceil(f-0.00001);
}

double floor_441(double f)
{
    return floor(f+0.00001);
}


vtkImageData *
NewImage(int width, int height)
{
    vtkImageData *img = vtkImageData::New();
    img->SetDimensions(width, height, 1);
    img->AllocateScalars(VTK_UNSIGNED_CHAR, 3);

    return img;
}

void
WriteImage(vtkImageData *img, const char *filename)
{
   std::string full_filename = filename;
   full_filename += ".png";
   vtkPNGWriter *writer = vtkPNGWriter::New();
   writer->SetInputData(img);
   writer->SetFileName(full_filename.c_str());
   writer->Write();
   writer->Delete();
}

class Triangle
{
  public:
      double         X[3];
      double         Y[3];
      double         Z[3];
      double         color[3][3];
};

class Screen
{
  public:
      unsigned char   *buffer;
      int width, height;

  // would some methods for accessing and setting pixels be helpful?
};

std::vector<Triangle>
GetTriangles(void)
{
    vtkPolyDataReader *rdr = vtkPolyDataReader::New();
    rdr->SetFileName("proj1d_geometry.vtk");
    cerr << "Reading" << endl;
    rdr->Update();
    cerr << "Done reading" << endl;
    if (rdr->GetOutput()->GetNumberOfCells() == 0)
    {
        cerr << "Unable to open file!!" << endl;
        exit(EXIT_FAILURE);
    }
    vtkPolyData *pd = rdr->GetOutput();
    int numTris = pd->GetNumberOfCells();
    vtkPoints *pts = pd->GetPoints();
    vtkCellArray *cells = pd->GetPolys();
    vtkFloatArray *var = (vtkFloatArray *) pd->GetPointData()->GetArray("hardyglobal");
    float *color_ptr = var->GetPointer(0);
    std::vector<Triangle> tris(numTris);
    vtkIdType npts;
    vtkIdType *ptIds;
    int idx;
    for (idx = 0, cells->InitTraversal() ; cells->GetNextCell(npts, ptIds) ; idx++)
    {
        if (npts != 3)
        {
            cerr << "Non-triangles!! ???" << endl;
            exit(EXIT_FAILURE);
        }
        tris[idx].X[0] = pts->GetPoint(ptIds[0])[0];
        tris[idx].X[1] = pts->GetPoint(ptIds[1])[0];
        tris[idx].X[2] = pts->GetPoint(ptIds[2])[0];
        tris[idx].Y[0] = pts->GetPoint(ptIds[0])[1];
        tris[idx].Y[1] = pts->GetPoint(ptIds[1])[1];
        tris[idx].Y[2] = pts->GetPoint(ptIds[2])[1];
        tris[idx].Z[0] = pts->GetPoint(ptIds[0])[2];
        tris[idx].Z[1] = pts->GetPoint(ptIds[1])[2];
        tris[idx].Z[2] = pts->GetPoint(ptIds[2])[2];
        // 1->2 interpolate between light blue, dark blue
        // 2->2.5 interpolate between dark blue, cyan
        // 2.5->3 interpolate between cyan, green
        // 3->3.5 interpolate between green, yellow
        // 3.5->4 interpolate between yellow, orange
        // 4->5 interpolate between orange, brick
        // 5->6 interpolate between brick, salmon
        double mins[7] = { 1, 2, 2.5, 3, 3.5, 4, 5 };
        double maxs[7] = { 2, 2.5, 3, 3.5, 4, 5, 6 };
        unsigned char RGB[8][3] = { { 71, 71, 219 },
                                    { 0, 0, 91 },
                                    { 0, 255, 255 },
                                    { 0, 128, 0 },
                                    { 255, 255, 0 },
                                    { 255, 96, 0 },
                                    { 107, 0, 0 },
                                    { 224, 76, 76 }
                                  };
        for (int j = 0 ; j < 3 ; j++)
        {
            float val = color_ptr[ptIds[j]];
            int r;
            for (r = 0 ; r < 7 ; r++)
            {
                if (mins[r] <= val && val < maxs[r])
                    break;
            }
            if (r == 7)
            {
                cerr << "Could not interpolate color for " << val << endl;
                exit(EXIT_FAILURE);
            }
            double proportion = (val-mins[r]) / (maxs[r]-mins[r]);
            tris[idx].color[j][0] = (RGB[r][0]+proportion*(RGB[r+1][0]-RGB[r][0]))/255.0;
            tris[idx].color[j][1] = (RGB[r][1]+proportion*(RGB[r+1][1]-RGB[r][1]))/255.0;
            tris[idx].color[j][2] = (RGB[r][2]+proportion*(RGB[r+1][2]-RGB[r][2]))/255.0;
        }
    }

    return tris;
}

void RasterizeGoingUpTriangle(Triangle triangle, unsigned char *buffer, int width, int height, double* zVals){

  double bleftX, bleftY, bleftZ; //base left
  double brightX, brightY, brightZ; //base right
  double topX, topY, topZ; //top of triangle
  double bleft_red, bleft_green, bleft_blue; //base left colors
  double bright_red, bright_green, bright_blue; //base right colors
  double topRed, topGreen, topBlue; //top colors
  double cur_leftRed, cur_leftGreen, cur_leftBlue; //current left colors
  double cur_rightRed, cur_rightGreen, cur_rightBlue; //current right colors
  double cur_red, cur_green, cur_blue; //current colors


  if(triangle.Y[0] == triangle.Y[1]){
    brightY = triangle.Y[0];
    bleftY  = triangle.Y[0];
    if(triangle.X[0] < triangle.X[1]){
      bleftX     = triangle.X[0];
      bleftZ     = triangle.Z[0];
      bleft_red   = triangle.color[0][0];
      bleft_green = triangle.color[0][1];
      bleft_blue  = triangle.color[0][2];

      brightX     = triangle.X[1];
      brightZ     = triangle.Z[1];
      bright_red   = triangle.color[1][0];
      bright_green = triangle.color[1][1];
      bright_blue  = triangle.color[1][2];
    }
    else{
      bleftX  = triangle.X[1];
      bleftZ  = triangle.Z[1];
      bleft_red   = triangle.color[1][0];
      bleft_green = triangle.color[1][1];
      bleft_blue  = triangle.color[1][2];

      brightX = triangle.X[0];
      brightZ = triangle.Z[0];
      bright_red   = triangle.color[0][0];
      bright_green = triangle.color[0][1];
      bright_blue  = triangle.color[0][2];
    }
    topX = triangle.X[2];
    topY = triangle.Y[2];
    topZ = triangle.Z[2];
    topRed   = triangle.color[2][0];
    topGreen = triangle.color[2][1];
    topBlue  = triangle.color[2][2];
  }
  else if(triangle.Y[0] == triangle.Y[2]){
    brightY = triangle.Y[0];
    bleftY  = triangle.Y[0];
    if(triangle.X[0] < triangle.X[2]){
      bleftX  = triangle.X[0];
      bleftZ  = triangle.Z[0];
      bleft_red   = triangle.color[0][0];
      bleft_green = triangle.color[0][1];
      bleft_blue  = triangle.color[0][2];

      brightX = triangle.X[2];
      brightZ = triangle.Z[2];
      bright_red   = triangle.color[2][0];
      bright_green = triangle.color[2][1];
      bright_blue  = triangle.color[2][2];
    }
    else{
      bleftX  = triangle.X[2];
      bleftZ  = triangle.Z[2];
      bleft_red   = triangle.color[2][0];
      bleft_green = triangle.color[2][1];
      bleft_blue  = triangle.color[2][2];

      brightX = triangle.X[0];
      brightZ = triangle.Z[0];
      bright_red   = triangle.color[0][0];
      bright_green = triangle.color[0][1];
      bright_blue  = triangle.color[0][2];
    }
    topX = triangle.X[1];
    topY = triangle.Y[1];
    topZ = triangle.Z[1];
    topRed   = triangle.color[1][0];
    topGreen = triangle.color[1][1];
    topBlue  = triangle.color[1][2];
  }
  else{
    brightY = triangle.Y[1];
    bleftY  = triangle.Y[1];
    if(triangle.X[1] < triangle.X[2]){
      bleftX  = triangle.X[1];
      bleftZ  = triangle.Z[1];
      bleft_red   = triangle.color[1][0];
      bleft_green = triangle.color[1][1];
      bleft_blue  = triangle.color[1][2];

      brightX = triangle.X[2];
      brightZ = triangle.Z[2];
      bright_red   = triangle.color[2][0];
      bright_green = triangle.color[2][1];
      bright_blue  = triangle.color[2][2];
    }
    else{
      bleftX  = triangle.X[2];
      bleftZ  = triangle.Z[2];
      bleft_red   = triangle.color[2][0];
      bleft_green = triangle.color[2][1];
      bleft_blue  = triangle.color[2][2];

      brightX = triangle.X[1];
      brightZ = triangle.Z[1];
      bright_red   = triangle.color[1][0];
      bright_green = triangle.color[1][1];
      bright_blue  = triangle.color[1][2];
    }
    topX = triangle.X[0];
    topY = triangle.Y[0];
    topZ = triangle.Z[0];
    topRed   = triangle.color[0][0];
    topGreen = triangle.color[0][1];
    topBlue  = triangle.color[0][2];
  }

  int ymin = ((bleftY < 0) ? 0 : ceil_441(bleftY));
  int ymax = ((topY >= height) ? (height - 1) : floor_441(topY));


  double* l_slope ;
  double* r_slope;

  if(topX == bleftX){
    l_slope = NULL;
  }
  else{
    double val1 = (topY - bleftY)/(topX - bleftX);
    l_slope = &val1;
  }
  if(topX == brightX){
    r_slope = NULL;
  }
  else{
    double val2 = (brightY - topY)/(brightX - topX);
    r_slope = &val2;
  }

  for(int i = ymin; i <= ymax; i++){
    int r_bound, l_bound, leftZ, rightZ;
    double cur_leftX, cur_rightX, cur_leftZ, cur_rightZ, cur_Z;

    cur_leftX = ((l_slope != NULL) ? bleftX + (((double) i) - bleftY)/(*l_slope) : bleftX);
    l_bound = ((cur_leftX < 0) ? 0 : ceil_441(cur_leftX));

    cur_rightX = ((r_slope != NULL) ? (brightX + (((double) i) - bleftY)/(*r_slope)) : brightX);
    r_bound = ((cur_rightX >= width) ? (width-1) : floor_441(cur_rightX));

    cur_leftZ = ((topZ==bleftZ) ? bleftZ : bleftZ + ((topZ - bleftZ)/(topY - bleftY)) * ((double) i - brightY));
    cur_leftRed = ((topRed == bleft_red) ? bleft_red : bleft_red + ((topRed - bleft_red)/(topY - bleftY)) * ((double) i - brightY));
    cur_leftGreen = ((topGreen == bleft_green) ? bleft_green : bleft_green + ((topGreen - bleft_green)/(topY - bleftY)) * ((double) i - brightY));
    cur_leftBlue = ((topBlue == bleft_blue) ? bleft_blue : bleft_blue + ((topBlue - bleft_blue)/(topY - bleftY)) * ((double) i - brightY));

    cur_rightZ = ((topZ == brightZ) ? brightZ : brightZ + ((topZ - brightZ)/(topY - brightY)) * ((double) i - brightY));
    cur_rightRed = ((topRed == bright_red) ? bright_red : bright_red + ((topRed - bright_red)/(topY - brightY)) * ((double) i - brightY));
    cur_rightGreen = ((topGreen == bright_green) ? bright_green : bright_green + ((topGreen - bright_green)/(topY-brightY))*((double)i - brightY));
    cur_rightBlue = ((topBlue == bright_blue) ? bright_blue : bright_blue + ((topBlue - bright_blue)/(topY - brightY))*((double) i - brightY));


    for(int j = l_bound; j <= r_bound; j++){
      cur_Z     = ((cur_rightZ - cur_leftZ)/(cur_rightX - cur_leftX))*((double) j - cur_leftX) + cur_leftZ;
      cur_red   = ((cur_rightRed - cur_leftRed)/(cur_rightX - cur_leftX))*((double) j - cur_leftX) + cur_leftRed;
      cur_green = ((cur_rightGreen - cur_leftGreen)/(cur_rightX - cur_leftX))*((double) j - cur_leftX) + cur_leftGreen;
      cur_blue  = ((cur_rightBlue - cur_leftBlue)/(cur_rightX - cur_leftX))*((double) j - cur_leftX) + cur_leftBlue;

      int pixel = width*i + j;
      int index = 3*pixel;

      if((cur_Z >= zVals[pixel]) && (cur_Z <= 0.0) && (cur_Z >= -1.0)){
        int r;
        int g;
        int b;
        r = ceil_441(255*cur_red);
        g = ceil_441(255*cur_green);
        b = ceil_441(255*cur_blue);

        buffer[index]     = r;
        buffer[index + 1] = g;
        buffer[index + 2] = b;

        zVals[pixel] = cur_Z;
      }
    }
  }
}

void RasterizeGoingDownTriangle(Triangle triangle, unsigned char *buffer, int width, int height, double* zVals){
  double bleftX, bleftY, bleftZ, brightX, brightY, brightZ, topX, topY, topZ;
  double bleft_red, bleft_green, bleft_blue, bright_red, bright_green, bright_blue, topRed, topGreen, topBlue;
  double cur_leftRed, cur_leftGreen, cur_leftBlue, cur_rightRed, cur_rightGreen, cur_rightBlue;
  double cur_red, cur_green, cur_blue;

  if(triangle.Y[0] == triangle.Y[1]){
    brightY = triangle.Y[0];
    bleftY  = triangle.Y[0];
    if(triangle.X[0] < triangle.X[1]){
      bleftX     = triangle.X[0];
      bleftZ     = triangle.Z[0];
      bleft_red   = triangle.color[0][0];
      bleft_green = triangle.color[0][1];
      bleft_blue  = triangle.color[0][2];

      brightX     = triangle.X[1];
      brightZ     = triangle.Z[1];
      bright_red   = triangle.color[1][0];
      bright_green = triangle.color[1][1];
      bright_blue  = triangle.color[1][2];
    }
    else{
      bleftX     = triangle.X[1];
      bleftZ     = triangle.Z[1];
      bleft_red   = triangle.color[1][0];
      bleft_green = triangle.color[1][1];
      bleft_blue  = triangle.color[1][2];

      brightX     = triangle.X[0];
      brightZ     = triangle.Z[0];
      bright_red   = triangle.color[0][0];
      bright_green = triangle.color[0][1];
      bright_blue  = triangle.color[0][2];
    }
    topX     = triangle.X[2];
    topY     = triangle.Y[2];
    topZ     = triangle.Z[2];
    topRed   = triangle.color[2][0];
    topGreen = triangle.color[2][1];
    topBlue  = triangle.color[2][2];
  }
  else if(triangle.Y[0] == triangle.Y[2]){
    brightY = triangle.Y[0];
    bleftY  = triangle.Y[0];
    if(triangle.X[0] < triangle.X[2]){
      bleftX     = triangle.X[0];
      bleftZ     = triangle.Z[0];
      bleft_red   = triangle.color[0][0];
      bleft_green = triangle.color[0][1];
      bleft_blue  = triangle.color[0][2];

      brightX     = triangle.X[2];
      brightZ     = triangle.Z[2];
      bright_red   = triangle.color[2][0];
      bright_green = triangle.color[2][1];
      bright_blue  = triangle.color[2][2];
    }
    else{
      bleftX     = triangle.X[2];
      bleftZ     = triangle.Z[2];
      bleft_red   = triangle.color[2][0];
      bleft_green = triangle.color[2][1];
      bleft_blue  = triangle.color[2][2];

      brightX     = triangle.X[0];
      brightZ     = triangle.Z[0];
      bright_red   = triangle.color[0][0];
      bright_green = triangle.color[0][1];
      bright_blue  = triangle.color[0][2];
    }
    topX = triangle.X[1];
    topY = triangle.Y[1];
    topZ = triangle.Z[1];
    topRed   = triangle.color[1][0];
    topGreen = triangle.color[1][1];
    topBlue  = triangle.color[1][2];
  }
  else{
    brightY = triangle.Y[1];
    bleftY  = triangle.Y[1];
    if(triangle.X[1] < triangle.X[2]){
      bleftX     = triangle.X[1];
      bleftZ     = triangle.Z[1];
      bleft_red   = triangle.color[1][0];
      bleft_green = triangle.color[1][1];
      bleft_blue  = triangle.color[1][2];

      brightX     = triangle.X[2];
      brightZ     = triangle.Z[2];
      bright_red   = triangle.color[2][0];
      bright_green = triangle.color[2][1];
      bright_blue  = triangle.color[2][2];
    }
    else{
      bleftX     = triangle.X[2];
      bleftZ     = triangle.Z[2];
      bleft_red   = triangle.color[2][0];
      bleft_green = triangle.color[2][1];
      bleft_blue  = triangle.color[2][2];

      brightX     = triangle.X[1];
      brightZ     = triangle.Z[1];
      bright_red   = triangle.color[1][0];
      bright_green = triangle.color[1][1];
      bright_blue  = triangle.color[1][2];
    }
    topX     = triangle.X[0];
    topY     = triangle.Y[0];
    topZ     = triangle.Z[0];
    topRed   = triangle.color[0][0];
    topGreen = triangle.color[0][1];
    topBlue  = triangle.color[0][2];
  }

  int ymin = ((topY < 0) ? 0 : ceil_441(topY));
  int ymax = ((bleftY >= height) ? (height - 1) : floor_441(bleftY));


  double* l_slope;
  double* r_slope;

  if(topX == bleftX){
    l_slope = NULL;
  }
  else{
    double val1 = (topY - bleftY)/(topX - bleftX);
    l_slope = &val1;
  }
  if(topX == brightX){
    r_slope = NULL;
  }
  else{
    double val2 = (brightY - topY)/(brightX - topX);
    r_slope = &val2;
  }

  for(int i = ymin; i <= ymax; i++){
    int r_bound, l_bound;
    double cur_leftX, cur_rightX, cur_leftZ, cur_rightZ, cur_Z;


    if(l_slope != NULL){
      double val1 = (((double) i) - topY)/(*l_slope);
      cur_leftX = topX + val1;
    }
    else{
      cur_leftX = bleftX;
    }

    l_bound = ((cur_leftX < 0) ? 0 : ceil_441(cur_leftX));

    if(r_slope != NULL){
      double val2 = (((double) i) - topY)/(*r_slope);
      cur_rightX = topX + val2;
    }
    else{
      cur_rightX = brightX;
    }

    r_bound = ((cur_rightX >= width) ? (width-1) : floor_441(cur_rightX));
    cur_leftZ = ((topZ == bleftZ) ? bleftZ : bleftZ + ((topZ - bleftZ)/(topY - bleftY)) * ((double) i - brightY));
    cur_leftRed = ((topRed == bleft_red) ? bleft_red : bleft_red + ((topRed - bleft_red) / (topY - bleftY)) * ((double) i - brightY));
    cur_leftGreen = ((topGreen == bleft_green) ? bleft_green : bleft_green + ((topGreen - bleft_green)/(topY - bleftY)) *((double) i - brightY));
    cur_leftBlue = ((topBlue == bleft_blue) ? bleft_blue : bleft_blue + ((topBlue - bleft_blue)/(topY - bleftY)) * ((double) i - brightY));

    cur_rightZ =((topZ == brightZ) ? brightZ : brightZ + ((topZ - brightZ)/(topY - brightY)) * ((double) i - brightY));
    cur_rightRed = ((topRed == bright_red) ? bright_red : bright_red + ((topRed - bright_red) / (topY - brightY)) * ((double) i - brightY));
    cur_rightGreen = ((topGreen == bright_green) ? bright_green : bright_green + ((topGreen - bright_green) / (topY - brightY)) * ((double) i - brightY));
    cur_rightBlue = ((topBlue == bright_blue) ? bright_blue : bright_blue + ((topBlue - bright_blue) / (topY - brightY)) * ((double) i - brightY));


    for(int j = l_bound; j <= r_bound; j++){
      cur_Z     = ((cur_rightZ - cur_leftZ)/(cur_rightX - cur_leftX))*(j - cur_leftX) + cur_leftZ;
      cur_red   = ((cur_rightRed - cur_leftRed)/(cur_rightX - cur_leftX))*(j - cur_leftX) + cur_leftRed;
      cur_green = ((cur_rightGreen - cur_leftGreen)/(cur_rightX - cur_leftX))*(j - cur_leftX) + cur_leftGreen;
      cur_blue  = ((cur_rightBlue - cur_leftBlue)/(cur_rightX - cur_leftX))*(j - cur_leftX) + cur_leftBlue;

      int pixel = width*i + j;

      int index = 3*pixel;

      if((cur_Z >= zVals[pixel]) && (cur_Z <= 0.0) && (cur_Z >= -1.0)){
        int r;
        int g;
        int b;
        r = ceil_441(255*cur_red);
        g = ceil_441(255*cur_green);
        b = ceil_441(255*cur_blue);

        buffer[index]     = r;
        buffer[index + 1] = g;
        buffer[index + 2] = b;

        zVals[pixel] = cur_Z;
      }
    }
  }

}

int main()
{
   int width = 1000;
   int height = 1000;
   vtkImageData *image = NewImage(width, height);
   unsigned char *buffer =
     (unsigned char *) image->GetScalarPointer(0,0,0);
   int npixels = width*height;
   for (int i = 0 ; i < npixels*3 ; i++)
       buffer[i] = 0;

   std::vector<Triangle> triangles = GetTriangles();
   double zVals[npixels];

   std::fill_n(zVals, npixels, -1.0);

   Screen screen;
   screen.buffer = buffer;
   screen.width = width;
   screen.height = height;

   double minX, midX, maxX;
   double minY, midY, maxY;
   double minZ, midZ, maxZ;
   double minRed, midRed, maxRed;
   double minGreen, midGreen, maxGreen;
   double minBlue, midBlue, maxBlue;

   int size = triangles.size();

  for(int j = 0; j<size; j++){

    Triangle triangle = triangles[j];


    if(triangle.Y[0] == triangle.Y[1]){
      if(triangle.Y[2] > triangle.Y[0]){
        RasterizeGoingUpTriangle(triangle, buffer, width, height, zVals);
      }
      else{
        RasterizeGoingDownTriangle(triangle, buffer, width, height, zVals);
      }
    }
    else if(triangle.Y[0] == triangle.Y[2]){
      if(triangle.Y[1] > triangle.Y[0]){
        RasterizeGoingUpTriangle(triangle, buffer, width, height, zVals);
      }
      else{
        RasterizeGoingDownTriangle(triangle, buffer, width, height, zVals);
      }
    }
    else if(triangle.Y[1] == triangle.Y[2]){
      if(triangle.Y[0] > triangle.Y[1]){
        RasterizeGoingUpTriangle(triangle, buffer, width, height, zVals);
      }
      else{
        RasterizeGoingDownTriangle(triangle, buffer, width, height, zVals);
      }
    }

    else{
      minY = triangle.Y[0];
      maxY = triangle.Y[0];
      midY = triangle.Y[0];
      minX = triangle.X[0];
      maxX = triangle.X[0];
      midX = triangle.X[0];
      minZ = triangle.Z[0];
      maxZ = triangle.Z[0];
      midZ = triangle.Z[0];
      maxRed = triangle.color[0][0];
      maxGreen = triangle.color[0][1];
      maxBlue = triangle.color[0][2];
      minRed = triangle.color[0][0];
      minGreen = triangle.color[0][1];
      minBlue = triangle.color[0][2];
      midRed = triangle.color[0][0];
      midGreen = triangle.color[0][1];
      midBlue = triangle.color[0][2];

      for(int i = 1; i<3; i++){
        if(triangle.Y[i] > maxY){
          maxY = triangle.Y[i];
          maxX = triangle.X[i];
          maxZ = triangle.Z[i];
          maxRed = triangle.color[i][0];
          maxGreen = triangle.color[i][1];
          maxBlue = triangle.color[i][2];
        }
        if(triangle.Y[i] < minY){
          minY = triangle.Y[i];
          minX = triangle.X[i];
          minZ = triangle.Z[i];
          minRed = triangle.color[i][0];
          minGreen = triangle.color[i][1];
          minBlue = triangle.color[i][2];
        }
      }

      for(int i = 0; i<3; i++){
        if((triangle.Y[i] < maxY) && (triangle.Y[i] > minY)){
          midY = triangle.Y[i];
          midX = triangle.X[i];
          midZ = triangle.Z[i];
          midRed = triangle.color[i][0];
          midGreen = triangle.color[i][1];
          midBlue = triangle.color[i][2];
        }
      }

      double newX;
      double newZ;
      double newRed;
      double newGreen;
      double newBlue;

      newX = minX + ((maxX - minX)/(maxY - minY))*(midY - minY);
      newZ = minZ + ((maxZ - minZ)/(maxY - minY))*(midY - minY);
      newRed = minRed + ((maxRed - minRed)/(maxY - minY))*(midY - minY);
      newGreen = minGreen + ((maxGreen - minGreen)/(maxY - minY))*(midY - minY);
      newBlue = minBlue + ((maxBlue - minBlue)/(maxY - minY))*(midY - minY);

      std::vector<Triangle> rv(2);

      Triangle triangle1 = rv[0];
      Triangle triangle2 = rv[1];

      triangle1.Y[0] = maxY;
      triangle1.X[0] = maxX;
      triangle1.Z[0] = maxZ;

      triangle1.Y[1] = midY;
      triangle1.X[1] = midX;
      triangle1.Z[1] = midZ;

      triangle1.Y[2] = midY;
      triangle1.X[2] = newX;
      triangle1.Z[2] = newZ;

      triangle1.color[0][0] = maxRed;
      triangle1.color[0][1] = maxGreen;
      triangle1.color[0][2] = maxBlue;
      triangle1.color[1][0] = midRed;
      triangle1.color[1][1] = midGreen;
      triangle1.color[1][2] = midBlue;
      triangle1.color[2][0] = newRed;
      triangle1.color[2][1] = newGreen;
      triangle1.color[2][2] = newBlue;

      triangle2.Y[0] = minY;
      triangle2.X[0] = minX;
      triangle2.Z[0] = minZ;

      triangle2.Y[1] = midY;
      triangle2.X[1] = midX;
      triangle2.Z[1] = midZ;

      triangle2.Y[2] = midY;
      triangle2.X[2] = newX;
      triangle2.Z[2] = newZ;

      triangle2.color[0][0] = minRed;
      triangle2.color[0][1] = minGreen;
      triangle2.color[0][2] = minBlue;
      triangle2.color[1][0] = midRed;
      triangle2.color[1][1] = midGreen;
      triangle2.color[1][2] = midBlue;
      triangle2.color[2][0] = newRed;
      triangle2.color[2][1] = newGreen;
      triangle2.color[2][2] = newBlue;


      RasterizeGoingUpTriangle(triangle1, buffer, width, height, zVals);

      RasterizeGoingDownTriangle(triangle2, buffer, width, height, zVals);
    }

   }
   printf("oh hey, your main function finished :)\n");

   WriteImage(image, "allTriangles");
}
