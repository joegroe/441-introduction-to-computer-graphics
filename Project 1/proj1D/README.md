### Project 1D - Removing Hidden Surfaces Using zbuffer Algorithm
* Interpolate colors within a triangle using zbuffer Algorithm


Result should look like:

![result](https://bitbucket.org/joegroe/441-introduction-to-computer-graphics/raw/315334099e9886e14bcc86645defae25d23d4eba/Project%201/proj1D/allTriangles.png)

##Usage
```bash
cmake .
make
open allTriangles.png
```
