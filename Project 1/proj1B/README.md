### Project 1B - Rasterize Screen-Space Triangles

Implementation of scanline algorithm to fill screen space with 100 "going down" triangles

Result should look like:

![result](https://bitbucket.org/joegroe/441-introduction-to-computer-graphics/raw/8335269e50db5a221a3725e173db5b0e9639d669/Project%201/differencer/100triangles.png)


##Usage
```bash
cmake .
make
open allTriangles.png
```
