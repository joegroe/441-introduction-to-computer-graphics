#include <iostream>
#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>

using std::cerr;
using std::endl;

double ceil_441(double f)
{
    return ceil(f-0.00001);
}

double floor_441(double f)
{
    return floor(f+0.00001);
}


vtkImageData *
NewImage(int width, int height)
{
    vtkImageData *img = vtkImageData::New();
    img->SetDimensions(width, height, 1);
    img->AllocateScalars(VTK_UNSIGNED_CHAR, 3);

    return img;
}

void
WriteImage(vtkImageData *img, const char *filename)
{
   std::string full_filename = filename;
   full_filename += ".png";
   vtkPNGWriter *writer = vtkPNGWriter::New();
   writer->SetInputData(img);
   writer->SetFileName(full_filename.c_str());
   writer->Write();
   writer->Delete();
}

class Triangle
{
  public:
      double         X[3];
      double         Y[3];
      unsigned char color[3];

};

class Screen
{
  public:
      unsigned char   *buffer;
      int width, height;

};

std::vector<Triangle>
GetTriangles(void)
{
   std::vector<Triangle> rv(100);

   unsigned char colors[6][3] = { {255,128,0}, {255, 0, 127}, {0,204,204},
                                  {76,153,0}, {255, 204, 204}, {204, 204, 0}};
   for (int i = 0 ; i < 100 ; i++)
   {
       int idxI = i%10;
       int posI = idxI*100;
       int idxJ = i/10;
       int posJ = idxJ*100;
       int firstPt = (i%3);
       rv[i].X[firstPt] = posI;
       if (i == 50)
           rv[i].X[firstPt] = -10;
       rv[i].Y[firstPt] = posJ+10*(idxJ+1);
       rv[i].X[(firstPt+1)%3] = posI+99;
       rv[i].Y[(firstPt+1)%3] = posJ+10*(idxJ+1);
       rv[i].X[(firstPt+2)%3] = posI+i;
       rv[i].Y[(firstPt+2)%3] = posJ;
       if (i == 5)
          rv[i].Y[(firstPt+2)%3] = -50;
       rv[i].color[0] = colors[i%6][0];
       rv[i].color[1] = colors[i%6][1];
       rv[i].color[2] = colors[i%6][2];
   }

   return rv;
}

void RasterizeGoingDownTriangle(Triangle triangle, unsigned char *buffer)
{
   double leftX;
   double leftY;
   double rightX;
   double rightY;
   double topX; 
   double topY;// "top" (really the bottom) of the triangle (the bottom point)

   if(triangle.Y[0] == triangle.Y[1])
   {
      rightY = triangle.Y[0];
      leftY  = triangle.Y[0];

      if(triangle.X[0] < triangle.X[1])
      {
         leftX  = triangle.X[0];
         rightX = triangle.X[1];
      }
      else{
         leftX  = triangle.X[1];
         rightX = triangle.X[0];
      }
       topX = triangle.X[2];
       topY = triangle.Y[2];
    }
    else if(triangle.Y[0] == triangle.Y[2])
    {
       rightY = triangle.Y[0];
       leftY  = triangle.Y[0];

       if(triangle.X[0] < triangle.X[2]){
          leftX  = triangle.X[0];
          rightX = triangle.X[2];
       }
       else{
          leftX  = triangle.X[2];
          rightX = triangle.X[0];
       }
       topX = triangle.X[1];
       topY = triangle.Y[1];
    }  
    else{
       rightY = triangle.Y[1];
       leftY  = triangle.Y[1];

       if(triangle.X[1] < triangle.X[2])
       {
          leftX  = triangle.X[1];
          rightX = triangle.X[2];
       }
       else{
          leftX  = triangle.X[2];
          rightX = triangle.X[1];
       }
       topX = triangle.X[0];
       topY = triangle.Y[0];
    }

    int ymin, ymax;
    //if topY > 0: set ymin equal to 0 else: set ymin equal to ceiling(topY)
    ymin = ((topY<0) ? 0 : ceil_441(topY)); 

    //if leftY > 999: set ymax = 999 else: set ymax to floor(leftY)
    ymax = ((leftY > 999) ? 999 : floor_441(leftY));


    double slope_l;
    double slope_r;

    // if topX == leftX: set left slope = 0.0. Else: set left slope equal to function
    slope_l = (( topX == leftX) ? 0.0 : ((topY - leftY)/(topX - leftX)));

    // if topX is equal to rightX: set right slope to 0.0. Else: set it equal to function.
    slope_r = (( topX == rightX) ? 0.0 : ((rightY - topY)/(rightX - topX)));


    for(int i = ymin; i <= ymax; i++){
       int r_bound;
       int l_bound;
       int cur_leftX;
       int cur_rightX;
       double tmp1;
       double tmp2;

       if(slope_l != 0.0){
          tmp1 = (((double) i) - topY)/(slope_l);
          cur_leftX = ceil_441(topX + tmp1);
       }
       else{
          cur_leftX = ceil_441(leftX);
       }

       //if the currently leftX point is negative, set to 0. otherwise set to current leftX point
       l_bound = ((cur_leftX < 0) ? 0 : cur_leftX);

       if(slope_r != 0.0){
          tmp2 = (((double) i) - topY)/(slope_r);
          cur_rightX = floor_441(topX + tmp2);
       }
       else{
          cur_rightX = floor_441(rightX);
       }

       //if current rightX point is greater than 999 (0-999), set it equal to 999. otherwise set to current leftX point
       r_bound = ((cur_rightX > 999) ? 999 : cur_rightX);

       for(int j = l_bound; j <= r_bound; j++){
          int index = 3*(1000*i + j); //special formula ;)
          //int index = 3*pix;    //dont forget the 3 colors!
          buffer[index] = triangle.color[0];    //r
          buffer[index + 1] = triangle.color[1];//g
          buffer[index + 2] = triangle.color[2];//b
        }  
    }     
}  

int main()
{
   vtkImageData *image = NewImage(1000, 1000);
   unsigned char *buffer =
     (unsigned char *) image->GetScalarPointer(0,0,0);
   int npixels = 1000*1000;
   for (int i = 0 ; i < npixels*3 ; i++)
       buffer[i] = 0;

   std::vector<Triangle> triangles = GetTriangles();

   Screen screen;
   screen.buffer = buffer;
   screen.width = 1000;
   screen.height = 1000;

   for(int j = 0; j<100; j++){ //Iterate 100 times for 100 triangles
       Triangle triangle = triangles[j];
       //send every triangle to function to create multiple triangles
       RasterizeGoingDownTriangle(triangle, buffer);
   }
   WriteImage(image, "allTriangles");
}
